import { Component, Entity, Event, System } from './types'
import { mapGetSet } from './utils/map'

let entities: Entity[] = []
let componentRegister: Map<string, Set<Component>> = new Map()

export function getAllEntities(): Entity[] {
  return entities.slice()
}

export function createNewEntity<E extends Entity>(fn: (id: number) => E): E {
  let id = entities.length
  let entity = fn(id)
  entities[id] = entity
  return entity
}

export function registerComponent(component: Component) {
  let componentSet = mapGetSet(componentRegister, component.type)
  componentSet.add(component)
}

export function unregisterComponent<C extends Component>(component: C) {
  let componentSet = mapGetSet(componentRegister, component.type)
  componentSet.delete(component)
}

export function getAllComponentsByType<C extends Component>(type: string): Set<C> {
  return mapGetSet(componentRegister, type) as Set<C>
}

let systems: System[] = []

export function registerSystem(system: System) {
  systems.push(system)
}

export function createNewSystem(run: () => void) {
  let system: System = { enabled: true, run }
  registerSystem(system)
  return system
}

let eventListenerRegister = new Map<string, Set<(event: Event) => void>>()

export function observeEvent<E extends Event>(type: string, fn: (event: E) => void) {
  let set = mapGetSet(eventListenerRegister, type)
  set.add(fn as (event: Event) => void)
}

export function publishEvent(event: Event) {
  let set = mapGetSet(eventListenerRegister, event.type)
  set.forEach(fn => fn(event))
}
