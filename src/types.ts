import { registerComponent, unregisterComponent } from './register'
import { getInstanceClassName } from './utils/class'

export class Entity {
  constructor(public readonly id: number) {
  }

  private readonly components = new Set<Component>()

  addComponent<C extends Component>(component: C) {
    this.components.add(component)
    registerComponent(component)
  }

  removeComponent(component: Component) {
    this.components.delete(component)
    unregisterComponent(component)
  }

  // override the JSON.stringify output
  toJSON() {
    return {
      id: this.id,
      components: Array.from(this.components),
    }
  }
}

export abstract class Component {
  get type(): string {
    return getInstanceClassName(this)
  }

  abstract get keys(): Array<keyof this>

  toJSON() {
    let res: Record<keyof this, this[keyof this]> & { type: string } = { type: this.type } as any
    for (let key of this.keys) {
      res[key] = this[key]
    }
    return res
  }
}

export interface System {
  enabled: boolean

  run(): void
}

export class Event {
  get type(): string {
    return getInstanceClassName(this)
  }
}
