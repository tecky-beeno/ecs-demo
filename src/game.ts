import {
  createNewSystem,
  getAllComponentsByType,
  createNewEntity,
  registerComponent,
  unregisterComponent,
  observeEvent,
  getAllEntities,
} from './register'
import { Component, Entity, Event } from './types'

export class Name extends Component {
  keys: Array<keyof this> = ['name']

  constructor(
    public name: string,
  ) {
    super()
  }
}

export class HP extends Component {
  keys: Array<keyof this> = [
    'hp',
  ]

  constructor(
    public hp: number,
    public defendRate: number,
  ) {
    super()
  }
}

export class AttackEvent extends Event {
  constructor(public attackerAtk: number, public defender: Life) {
    super()
  }
}

export function createLife(name: string, hp: number, defendRate: number = 1) {
  return createNewEntity(id => new Life(
    id,
    new Name(name),
    new HP(hp, defendRate),
  ))
}

export function createHuman(name: string) {
  return createLife(name, 100)
}

export let ReportSystem = createNewSystem(() => {
  console.log('== begin report ==')
  console.log('timestamp:', new Date().toISOString())
  for (let entity of getAllEntities()) {
    console.log(JSON.stringify(entity))
  }
  console.log('== end report ==')
})
observeEvent<AttackEvent>(AttackEvent.name, event => {
  event.defender.hp.hp -= event.attackerAtk * event.defender.hp.defendRate
})


export function fireEvent<C extends Component>(event: C) {
  registerComponent(event)
}

export class Weapon extends Entity {

}

export class Life extends Entity {
  constructor(
    id: number,
    public name: Name,
    public hp: HP,
  ) {
    super(id)
    this.addComponent(name)
    this.addComponent(hp)
  }
}
