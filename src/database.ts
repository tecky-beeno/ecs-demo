import { Entity } from './types'
import { getInstanceClassName } from './utils/class'

export function saveEntity(entity: Entity) {
  let className = getInstanceClassName(entity)
  // console.log('class:', className)
  // console.log('id:', entity.id)
  // console.log('cs', entity.componentSets)
  let setFields: string[] = []
  let fields: any = {}
  let { id, components } = entity.toJSON()
  for (let component of components) {
    for (let key of component.keys) {
      let value = component[key]
      fields[key] = value
      setFields.push(`${key} = ${value}`)
    }
  }
  console.log('knex update:', { table: className, id, fields })
  console.log(`update ${className}
set ${setFields.join(', ')}
where id = ${id}`)
}
