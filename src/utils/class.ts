export interface ClassConstructor<C> extends Function {
  new(...args: any[]): C
}

export function getInstanceClassName(instance: object) {
  return instance.constructor.name
}
