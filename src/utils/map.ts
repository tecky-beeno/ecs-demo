export function mapGetSet<K, V>(map: Map<K, Set<V>>, key: K): Set<V> {
  if (map.has(key)) {
    return map.get(key)!
  }
  let set = new Set<V>()
  map.set(key, set)
  return set
}
