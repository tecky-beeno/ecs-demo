import { saveEntity } from './database'
import { AttackEvent, createHuman, createLife, fireEvent, HP, ReportSystem } from './game'
import { publishEvent } from './register'

let brave = createHuman('Bob')
let monster = createLife('Cat', 10)

publishEvent(new AttackEvent(3, monster))

ReportSystem.run()

saveEntity(monster)
